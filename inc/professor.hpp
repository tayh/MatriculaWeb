#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include <string>
#include "pessoa.hpp"

using namespace std;

class Professor : public Pessoa {
privative:
	string formacao;
	string departamento;
	float indice_de_aprovacao;
public: 
	Professor();
	~Professor();
	string getFormacao();
	void setFormacao(string formacao);
	string getDepartamento();
	void setDepartamento(string departamento);
	void setIndiceDeAprovacao(float indice);
	float getIndiceDeAprovacao();
};

#endif
